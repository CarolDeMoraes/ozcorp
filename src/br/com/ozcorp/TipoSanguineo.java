package br.com.ozcorp;
/**
 * 
 * @author Carolina de Moraes Josephik
 *
 */
public enum TipoSanguineo {
	APOSITIVO("A+"), ANEGATIVO("A-"), BPOSITIVO("B+"), BNEGATIVO("B-"), ABPOSITIVO("AB+"), ABNEGATIVO("AB-"), OPOSITIVO("O+"), ONEGATIVO("O-");

public String s;
	
	TipoSanguineo (String s){
		this.s=s;
	}

}
