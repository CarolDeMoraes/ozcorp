package br.com.ozcorp;
/**
 * 
 * @author Carolina de Moraes Josephik
 *
 */
public class Funcionarios {

	// atributos
	private String nome;
	private String rg;
	private String cpf;
	private String matricula;
	private String email;
	private Cargo cargo;
	private String senha;
	private Sexo sexo;
	private TipoSanguineo tipoS;
	private Departamento departamento;
	private int nivelAcesso;

	

	// construtor
	public Funcionarios(String nome, String rg, String cpf, String matricula, String email, String senha,
			Sexo sexo, TipoSanguineo tipoS, Departamento departamento, int nivelAcesso) {
		
		this.nome = nome;
		this.rg = rg;
		this.cpf = cpf;
		this.matricula = matricula;
		this.email = email;
		this.senha = senha;
		this.sexo = sexo;
		this.tipoS = tipoS;
		this.departamento = departamento;
		this.nivelAcesso = nivelAcesso;
	}


	//getters and setters
	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
	
	public Cargo getCargo() {
		return cargo;
	}

	public void setDepartamento(Cargo cargo) {
		this.cargo= cargo;
		
	}


	public Sexo getSexo() {
		return sexo;
	}


	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}


	public TipoSanguineo getTipoS() {
		return tipoS;
	}


	public void setTipoS(TipoSanguineo tipoS) {
		this.tipoS = tipoS;
	}


	public Departamento getDepartamento() {
		return departamento;
	}


	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}


	public int getNivelAcesso() {
		return nivelAcesso;
	}


	public void setNivelAcesso(int nivelAcesso) {
		this.nivelAcesso = nivelAcesso;
	}


	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	
	

}
