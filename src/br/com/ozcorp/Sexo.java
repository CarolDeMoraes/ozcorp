package br.com.ozcorp;
/**
 * 
 * @author Carolina de Moraes Josephik
 *
 */
public enum Sexo {
	MSACULINO("Masculino"), FEMININO("Feminino"), OUTRO("outro");
	
	public String s;
	
	Sexo(String s){
		this.s=s;
		
	}

}

